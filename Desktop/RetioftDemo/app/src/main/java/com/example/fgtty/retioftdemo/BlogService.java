package com.example.fgtty.retioftdemo;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by fgtty on 2017/8/10.
 */

public interface BlogService {

    //09aac837f38f4e67b29661f53f14ed5a
//    @FormUrlEncoded
//    @POST("book/reviews")
//    Call<String> addReviews(@Field("book") String bookId, @Field("title") String title,
//                            @Field("content") String content, @Field("rating") String rating);
//    @FormUrlEncoded
////    @POST("api/getPostingOrderByDrvId.htm")
////    Call<String> addReviews(@Field("drvIds") String drvId
////    );
//    @POST("api/getPostingOrderByDrvId.htm")
//    Observable<String> mGetLoginAPI(@Query("username") String username,
//                                    @Query("password") String password);
    @Multipart
    @POST("photo/drvImageUpload.htm")
    Call<BlogService> updateImage(@PartMap Map<String, RequestBody> params);
}
