package com.example.fgtty.retioftdemo;

/**
 * Created by fgtty on 2017/8/10.
 */

class ResultBean {

    /**
     * success : true
     * msgType : 1
     * msg : 上传成功
     * obj : null
     */

    private boolean success;
    private String msgType;
    private String msg;
    private Object obj;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}
